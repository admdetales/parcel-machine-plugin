<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Tests\Omni\Sylius\ParcelMachinePlugin\Behat\Context\Setup;

use Behat\Behat\Context\Context;
use Omni\Sylius\ParcelMachinePlugin\Model\ParcelMachineInterface;
use Sylius\Bundle\CoreBundle\Fixture\Factory\ExampleFactoryInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

class ParcelMachineContext implements Context
{
    /**
     * @var ExampleFactoryInterface
     */
    private $factory;

    /**
     * @var RepositoryInterface
     */
    private $repository;

    /**
     * @param ExampleFactoryInterface $factory
     * @param RepositoryInterface $repository
     */
    public function __construct(ExampleFactoryInterface $factory, RepositoryInterface $repository)
    {
        $this->factory = $factory;
        $this->repository = $repository;
    }

    /**
     * @Given the store has a parcel machine with code :code
     */
    public function storeHasAParcelMachineWithCode(string $code)
    {
        /** @var ParcelMachineInterface $parcelMachine */
        $parcelMachine = $this->factory->create(['code' => $code]);
        $this->repository->add($parcelMachine);
    }
}
