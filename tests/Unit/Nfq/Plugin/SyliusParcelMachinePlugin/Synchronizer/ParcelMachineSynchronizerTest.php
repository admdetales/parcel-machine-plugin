<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Tests\Omni\Sylius\ParcelMachinePlugin\Unit\Omni\Sylius\ParcelMachinePlugin\Synchronizer;

use Doctrine\ORM\EntityManagerInterface;
use Omni\Sylius\ParcelMachinePlugin\Doctrine\ORM\ParcelMachineRepositoryInterface;
use Omni\Sylius\ParcelMachinePlugin\Model\ParcelMachine;
use Omni\Sylius\ParcelMachinePlugin\Model\ParcelMachineInterface;
use Omni\Sylius\ParcelMachinePlugin\Provider\ParcelMachineProviderInterface;
use Omni\Sylius\ParcelMachinePlugin\Synchronizer\ParcelMachineSynchronizer;
use Omni\Sylius\ParcelMachinePlugin\Synchronizer\ParcelMachineSynchronizerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ProphecyInterface;

class ParcelMachineSynchronizerTest extends TestCase
{
    /**
     * @var ParcelMachineProviderInterface|MockObject
     */
    private $provider;

    /**
     * @var EntityManagerInterface|ProphecyInterface
     */
    private $em;

    /**
     * @var ParcelMachineRepositoryInterface|MockObject
     */
    private $repository;

    /**
     * @var ParcelMachineSynchronizerInterface
     */
    private $synchronizer;

    protected function setUp()
    {
        $this->em = $this->prophesize(EntityManagerInterface::class);
        $this->repository = $this->createMock(ParcelMachineRepositoryInterface::class);
        $this->provider = $this->createMock(ParcelMachineProviderInterface::class);

        $this->synchronizer = new ParcelMachineSynchronizer($this->em->reveal(), $this->repository);
    }

    public function testSkipsSynchronizationWhenProviderReturnsAnEmptyList()
    {
        $this->provider->method('getAll')->willReturn([]);

        $this->em->flush()->shouldNotBeCalled();

        $this->synchronizer->synchronize($this->provider);
    }

    public function testSynchronizationDisablesExtraAddresses()
    {
        $this->provider->method('getAll')->willReturn(
            [
                $this->createParcelMachine('DPD001'),
                $this->createParcelMachine('DPD002'),
            ]
        );

        $toBeDisabled = $this->createParcelMachine('DPD003');

        $this->repository->method('findByProvider')->willReturn(
            [
                'DPD001' => $this->createParcelMachine('DPD001'),
                'DPD002' => $this->createParcelMachine('DPD002'),
                'DPD003' => $toBeDisabled
            ]
        );

        $this->em->flush()->shouldBeCalled();

        $this->synchronizer->synchronize($this->provider);
        $this->assertFalse($toBeDisabled->isEnabled());
    }

    public function testSynchronizationAddsMissingAddresses()
    {
        $toBeAdded = $this->createParcelMachine('DPD0003');

        $this->provider->method('getAll')->willReturn(
            [
                $this->createParcelMachine('DPD001'),
                $this->createParcelMachine('DPD002'),
                $toBeAdded
            ]
        );


        $this->repository->method('findByProvider')->willReturn(
            [
                'DPD001' => $this->createParcelMachine('DPD001'),
                'DPD002' => $this->createParcelMachine('DPD002'),
            ]
        );

        $this->em->persist($toBeAdded)->shouldBeCalled();
        $this->em->flush()->shouldBeCalled();

        $this->synchronizer->synchronize($this->provider);
    }

    public function testSynchronizationAddsAndDisablesAddresses()
    {
        $toBeAdded = $this->createParcelMachine('DPD0001');

        $this->provider->method('getAll')->willReturn(
            [
                $toBeAdded,
                $this->createParcelMachine('DPD002')
            ]
        );

        $toBeDisabled = $this->createParcelMachine('DPD003');

        $this->repository->method('findByProvider')->willReturn(
            [
                'DPD002' => $this->createParcelMachine('DPD002'),
                'DPD003' => $toBeDisabled
            ]
        );

        $this->em->persist($toBeAdded)->shouldBeCalled();
        $this->em->flush()->shouldBeCalled();

        $this->synchronizer->synchronize($this->provider);
        $this->assertFalse($toBeDisabled->isEnabled());
    }

    public function testSynchronizationUpdatesExistingAddressInformation()
    {
        $original = $this->createParcelMachine('DPD001');
        $original->setCity('Kaunas');

        $this->provider->method('getAll')->willReturn([$original]);

        $toBeUpdated = $this->createParcelMachine('DPD001');
        $this->repository->method('findByProvider')->willReturn(['DPD001' => $toBeUpdated]);

        $this->synchronizer->synchronize($this->provider);

        $this->assertEquals($original->getCity(), $toBeUpdated->getCity());
    }

    /**
     * @param string $code
     *
     * @return ParcelMachineInterface
     */
    private function createParcelMachine(string $code): ParcelMachineInterface
    {
        $parcelMachine = new ParcelMachine();

        $parcelMachine->setCode($code);
        $parcelMachine->setCountry('LT');
        $parcelMachine->setCity('Vilnius');
        $parcelMachine->setStreet('Konstitucijos pr. 7');

        return $parcelMachine;
    }
}
