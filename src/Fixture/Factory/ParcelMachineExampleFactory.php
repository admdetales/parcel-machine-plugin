<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ParcelMachinePlugin\Fixture\Factory;

use Omni\Sylius\ParcelMachinePlugin\Model\ParcelMachineInterface;
use Sylius\Bundle\CoreBundle\Fixture\Factory\AbstractExampleFactory;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParcelMachineExampleFactory extends AbstractExampleFactory
{
    /**
     * @var FactoryInterface
     */
    private $parcelMachineFactory;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    /**
     * @var OptionsResolver
     */
    private $optionsResolver;

    /**
     * @param FactoryInterface $parcelMachineFactory
     */
    public function __construct(FactoryInterface $parcelMachineFactory) {
        $this->parcelMachineFactory = $parcelMachineFactory;

        $this->faker = \Faker\Factory::create();
        $this->optionsResolver = new OptionsResolver();

        $this->configureOptions($this->optionsResolver);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefault('code', function (Options $options): string {
                return $this->faker->postcode;
            })
            ->setDefault('country', function (Options $options): string {
                return 'LT';
            })
            ->setDefault('city', function (Options $options): ?string {
                return $this->faker->city;
            })
            ->setDefault('street', function (Options $options): ?string {
                return $this->faker->streetAddress;
            })
            ->setDefault('provider', function (Options $options): string {
                return 'dpd';
            })
            ->setDefault('enabled', function (Options $options): bool {
                return true;
            })
            ->setAllowedTypes('country', ['string']);
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $options = []): ParcelMachineInterface
    {
        $options = $this->optionsResolver->resolve($options);

        /** @var ParcelMachineInterface $parcelMachine */
        $parcelMachine = $this->parcelMachineFactory->createNew();
        $parcelMachine->setCode($options['code']);
        $parcelMachine->setCity($options['city']);
        $parcelMachine->setStreet($options['street']);
        $parcelMachine->setProvider($options['provider']);
        $parcelMachine->setEnabled($options['enabled']);
        $parcelMachine->setCountry($options['country']);

        return $parcelMachine;
    }
}
