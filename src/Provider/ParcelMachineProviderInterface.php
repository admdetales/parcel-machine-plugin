<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ParcelMachinePlugin\Provider;

use Omni\Sylius\ParcelMachinePlugin\Model\ParcelMachineInterface;

interface ParcelMachineProviderInterface
{
    /**
     * @return ParcelMachineInterface[]
     */
    public function getAll(): array;

    /**
     * @param string $code
     *
     * @return ParcelMachineInterface[]
     */
    public function getByCountry(string $code): array;

    /**
     * @return string
     */
    public function getCode(): string;
}
