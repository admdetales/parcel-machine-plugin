<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ParcelMachinePlugin\Menu;

use Knp\Menu\ItemInterface;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

class ParcelMachineMenuBuilder
{
    /**
     * @param MenuBuilderEvent $event
     */
    public function buildMenu(MenuBuilderEvent $event)
    {
        /** @var ItemInterface|null $group */
        $group = $event->getMenu()->getChild('configuration');

        if (null === $group) {
            return;
        }

        $group
            ->addChild('parcel_machines', ['route' => 'omni_admin_parcel_machine_index'])
            ->setLabel('omni.ui.parcel_machines')
            ->setLabelAttribute('icon', 'cloud');
    }
}
